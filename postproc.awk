BEGIN {
	if (!filename)
		filename = "a";
}
function out(text) {
	print text >> (filename "." state)
}
function downh(num) {
	num=substr($0, 3, 1)
	sub(/<h[0-9]>/,("<h" (num+1) ">"))
	sub(/<\/h[0-9]>/,("</h" (num+1) ">"))
}
state == 0 && /<body>/ { state = 1; next }
state == 0 { next }

state == 1 && /<h1>/ {
	gsub(/\\-/,"-")
	downh()
	out($0);
	next
}
state == 1 && /<hr>/ {
	out("<hr>");
	state = 2;
	next
}
state == 1 { next }

state == 2 && /<h2>Index<\/h2>/ {
	state = 3;
	out($0);
	next
}
# man2html is unable to handle commands like:
#  .IP \n[step].
# Instead of evaluating \n[step], it strips "\n[" and leaves the rest
# untouched.  The klugde below is based on the assumption that we always
# use step as the name of the variable.
state == 2 && /<dl/ { counter=1 }
state == 2 && /<dt>step\]\./ {
	sub(/<dt>step\]\./,("<dt>" counter "."))
	++counter
}

state == 2 && /<h2>COMMANDS<\/h2>/ { command_links=1; out($0); next }
state == 2 && /<h2>/ { command_links=0 }
state == 2 && command_links == 1 && /^<b>.*<\/b>[,.]$/ {
	punct=substr($0,length($0))
	sub(/<b>/,"")
	sub(/<\/b>[,.]/,"")
	out("<b><a href=\"eclat-" $0 ".html\">" $0 "</a></b>" punct)
	if (punct == ".")
		command_links=0
	next
}

state == 3 && /<hr>/ {
	out($0);
	state = 4;
	next
}

state == 4 {
	out("<p>This document was created by <a href=\"http://primates.ximian.com/~flucifredi/man/\">man2html</a> and postprocessed by postproc.awk.</p>")
	out("<p>" strftime("%c", systime()) "</p>")
	exit
}

/<h[0-9]>/ { downh() }

{
	s=gensub(/HREF="\.\.\/man[1-8n]\/(eclat[a-z.-]*)\.[1-8n]\.html/,
  	         "href=\"\\1.html", "g")
	s=gensub(/HREF="\.\.\/man[1-8n]\/(forlan[a-z.-]*)\.[1-8n]\.html/,
  	         "href=\"\\1.html", "g",s)
	s=gensub(/HREF="\.\.\/man([1-8n])\/(.+)\.[1-8n]\.html/,
		 "href=\"http://www.manpagez.com/man/\\1/\\2/","g",s)
	out(s)
}
	
	